extern crate rand;

use rand::seq::SliceRandom;
use std::fmt;

const STATS: [&'static str; 6] = [
    "STR",
    "DEX",
    "CON",
    "INT",
    "WIS",
    "CHA"
];

const APPEARANCES: [&'static str; 20] = [
    "Distinctive jewelry: earrings, necklace, circlet, bracelets",
    "Piercings",
    "Flamboyant or outlandish clothes",
    "Formal, clean clothes",
    "Ragged, dirty clothes",
    "Pronouonced scar",
    "Missing teeth",
    "Missing fingers",
    "Unusual eye color (or two different eye colors",
    "Tattoos",
    "Birthmark",
    "Unusual skin color",
    "Bald",
    "Braided beard or hair",
    "Unusual hair color",
    "Nervous eye twitch",
    "Distinctive nose",
    "Distinctive posture (crooked or rigid)",
    "Exceptionally beautiful",
    "Exceptionally ugly"
];

const TALENTS: [&'static str; 20] = [
    "Plays a musical instrument",
    "Speaks several languages",
    "Unbelievably lucky",
    "Perfect memory",
    "Great with animals",
    "Great with children",
    "Great at solving puzzles",
    "Great at one game",
    "Great at impressions",
    "Draws beautifully",
    "Paints beautifully",
    "Sings beautifully",
    "Drinks everyone under the table",
    "Expert carpenter",
    "Expert cook",
    "Expert dart thrower and rock skipper",
    "Expert juggler",
    "Skilled actor and master of disguise",
    "Skilled dancer",
    "Knows theives' cant"
];

struct NPC<'a> {
    appearance: &'a str,
    high_stat: &'a str,
    low_stat: &'a str,
    talent: &'a str,
}

impl<'a> fmt::Display for NPC<'a> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Appearance: {}\n\
                   High Stat: {}\n\
                   Low Stat: {}\n\
                   Talent: {}",
            self.appearance, self.high_stat, self.low_stat, self.talent)
    }
}

fn main() {
    let mut rng = rand::thread_rng();

    let high_stat = STATS.choose(&mut rng).unwrap();
    let low_stat = loop {
        // Add a context in the loop so we don't shadow a bunch and clean up every cycle.
        {
            let stat = STATS.choose(&mut rng).unwrap();
            if stat != high_stat {
                break stat;
            }
        }
    };


    let s = NPC {
        appearance: APPEARANCES.choose(&mut rng).unwrap(),
        high_stat: high_stat,
        low_stat: low_stat,
        talent: TALENTS.choose(&mut rng).unwrap()
    };
    println!("{}", s)
}
